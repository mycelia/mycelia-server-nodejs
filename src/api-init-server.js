const config = require('./api-config').config;

// choose port

//load server on selected port
const Hapi = require('hapi');
const srv = new Hapi.Server();
srv.connection({
	port: config.port || 0,
	host: config.host || "127.0.0.1"
});

module.exports = srv;
