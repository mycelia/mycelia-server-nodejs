const app = require('./auth-logout');

function replySpyMock_builder(spyObject){
	return json => {
		spyObject.reply = json;
		spyObject.reply.state = (a,b,c)=>{
			spyObject.reply.stateRes = {cookieName:a,cookieContent:b,cookieOptions:c};
			return spyObject.reply;
		};
		return spyObject.reply;
	};
}
describe('auth-logout',()=>{
	it('log you out', () => {
		const spy = {};
		app.route.handler({},replySpyMock_builder(spy));
		expect(spy.reply.stateRes.cookieContent).toBeFalsy();
		expect(spy.reply.stateRes.cookieOptions.ttl).toBe(1);
	});
});
