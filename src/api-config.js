const fs = require('fs');
let dataFolder = "data.example/";
if (fs.existsSync("data.dev/")) dataFolder = "data.dev/";
if (process.env.DATA_FOLDER) dataFolder = process.env.DATA_FOLDER;
if (process.dataFolder) dataFolder = process.dataFolder;
if (process.options && process.options.dataFolder) dataFolder = process.options.dataFolder;

if(dataFolder[dataFolder.length-1] !== '/') dataFolder += '/';

const yaml = require('js-yaml');
const config = merge(
	yaml.safeLoad(fs.readFileSync(dataFolder + "config-srv.yml", 'utf8')),
	process.options
);
config.dataFolder = dataFolder;
if (process.argv[2]) config.port = parseInt(process.argv[2]);
if (process.env.PORT) config.port = parseInt(process.env.PORT);
if (process.env.HOST) config.host = process.env.HOST;

module.exports.config = config;
module.exports.users = yaml.safeLoad(fs.readFileSync(dataFolder + "users.yml", 'utf8'));


function clone(json) {
	return JSON.parse(JSON.stringify(json))
}

function merge(baseJson, overwritingJson) {
	if(typeof baseJson !== "object") return clone(overwritingJson);
	let res = clone(baseJson);
	for (let key in overwritingJson) {
		if (typeof overwritingJson[key] === "object" && typeof res[key] !== 'undefined') {
			res[key] = merge(res[key], overwritingJson[key]);
		}
		else res[key] = overwritingJson[key];
	}
	return res;
}
