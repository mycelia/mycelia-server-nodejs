/*
 - [x] test si le dépot git est initialisé et remonter le problème à l'utilisateur si ce n'est pas le cas.
 - [x] dans les test préparer un environnement git initialisé
 - FIXME: hors MVP mais à faire au plus vite : test qu'une fonction de validation des données est appelée si disponible
 - [x] test si l'utilisateur à le droit d'ajouter le fichier
 - [x] test l'ajout/modification de fichier depuis un commit spécifique
 - [x] valide le commit au nom de l'éméteur
 - [x] test le pull/merge avec résolution des conflits triviaux et envoi d'erreur en cas d'échec (TODO: avec email de notif a l'admin).
 - [x] test le push
 - [x] brancher persist.js et accessRights.js sur l'api.
 - (vérifier que le force-push n'est pas accepté)

 coté mycelia-cli
 //await git(config.dataFolder).addConfig('receive.denyCurrentBranch','updateInstead'); //TODO: add this in mycelia-cli init func
 - tester l'initialisation d'un projet à partir d'un dossier modèle
 action de : mycelia-cli deploy (appellé par gitlab-ci)
 - build mycelia-front-app avec les données, en version "pour la prod"
 - vérifie que le build est fonctionnel (test e2e de chargement à minima)
 - publie sur chaque serveur de visualisation listé (tester pour un dossier local aussi bien que vers un serveur distant)


 */


const commitRoutes = [
	{
		method: "PUT", path: '/commit/node/{id}/{content}',
		config: {
			auth: 'jwt',
			description: "per node update with fine access right",
			tags: ["registered only", "node dependent access right", "TODO"]
		},
		handler: function (request, reply) {
			reply({text: 'Saved'})
				.header("Authorization", request.headers.authorization);
		}
	},
	{
		method: "PUT", path: '/commit/link/{id}/{content}',
		config: {
			auth: 'jwt',
			description: "per link update with fine access right",
			tags: ["registered only", "link dependent access right", "TODO"]
		},
		handler: function (request, reply) {
			reply({text: 'Saved'})
				.header("Authorization", request.headers.authorization);
		}
	},
	{
		method: "DELETE", path: '/commit/node/{id}',
		config: {
			auth: 'jwt',
			description: "per node delete with fine access right",
			tags: ["registered only", "node dependent access right", "TODO"]
		},
		handler: function (request, reply) {
			reply({text: 'Saved'})
				.header("Authorization", request.headers.authorization);
		}
	},
	{
		method: "DELETE", path: '/commit/link/{id}',
		config: {
			auth: 'jwt',
			description: "per link delete with fine access right",
			tags: ["registered only", "link dependent access right", "TODO"]
		},
		handler: function (request, reply) {
			reply({text: 'Saved'})
				.header("Authorization", request.headers.authorization);
		}
	},
	{
		method: "PATCH", path: '/commit/{hash}/{files*}',
		config: {
			auth: 'jwt',
			description: "apply sent diff and commit as {name}@email.ml if you have write access for theses files",
			tags: ["registered only", "files dependent access right", "TODO"]
		},
		handler: function (request, reply) {
			reply({text: 'Saved'})
				.header("Authorization", request.headers.authorization);
		}
	},
	{
		method: "DELETE", path: '/commit/{files*}',
		config: {auth: 'jwt', tags: ["registered only", "files dependent access right", "TODO"]},
		handler: function (request, reply) {
			reply({text: 'Deleted'}).header("Authorization", request.headers.authorization);
		}
	}
];
module.exports = commitRoutes;
