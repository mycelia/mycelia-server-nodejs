const JWT = require('jsonwebtoken');
const email = require('emailjs');
const ms = require('ms');
const config = require('./api-config').config;
const srv = require('./api-init-server');
//const Joi = require('joi');

const handler = function (request, reply) {
	const userEmail = request.params.email || request.payload.email; // valider le format
	const data = {email: userEmail};
	if (request.params.b64url) data.url = (new Buffer(request.params.b64url, 'base64')).toString();
	if (request.payload.url) data.url = request.payload.url;
	const token = JWT.sign(data, config.secretKey, {expiresIn: config.email.ttl});

	const emailSrv = email.server.connect({
		user: config.email.user,
		password: config.email.password,
		host: config.email.host,
		tls: true
	});
	if (!config.email.sender) config.email.sender = config.email.user;
	if (!config.email.replyTo) config.email.replyTo = config.email.sender;
	const url = request.connection.info.protocol + '://' + request.info.host + "/auth/jwt/" + token;
	if(config.logEmailToken) console.log(url);
	const expires = new Date();
	expires.setTime(Date.now() + ms(config.email.ttl));
	//"EEE MMM  d HH:mm:ss yyyy Z"
	const rawEmailContent = "Voici le lien d'authentification que vous venez de demander pour vous connecter sur Mycelia : "
		+ url + "\n"
		+ 'cliquez dessus ou copiez le dans la barre d\'adresse de votre navigateur pour vous connecter.\n'
		+ "Ce lien, valable 10 minutes, est à usage unique.\n\n\n"
		+ "Pour plus de sécurité Mr Bond, ce lien s'autodétruira après sa première utilisation, ou dans 10 minutes sinon."
		+ "Mais cet email ne sera pas supprimé automatiquement pour autant.";
	const message = email.message.create({
		text: rawEmailContent,
		from: config.email.sender,
		to: userEmail,
		"reply-to": config.email.replyTo,
		Expires: expires,
		subject: "Confirmez votre connexion — Mycelia",
		attachment: [
			{
				data: "Voici le lien d'authentification que vous venez de demander pour vous connecter sur Mycelia : <br/>"
				+ '<h1><a href="' + url + '" style="display: block;color: #dcf69d; background: #7c9899; padding: 20px; margin: 20px 200px; width: 150px; text-align: center; text-decoration: none; border-radius: 10px; font-weight: bold;">Connexion</a></h1>'
				+ '<a href="' + url + '">' + url + '</a><br/>'
				+ 'cliquez dessus ou copiez le dans la barre d\'adresse de votre navigateur pour vous connecter.<br/>'
				+ "Ce lien, valable 10 minutes, est à usage unique.<br/><br/><br/>"
				+ "Pour plus de sécurité Mr Bond, ce lien s'autodétruira après sa première utilisation, ou dans 10 minutes sinon. Mais cet email ne sera pas supprimé automatiquement pour autant."
				, alternative: true
			}
		]
	});
	emailSrv.send( message, (err, message)=> {
		if(err){
			console.log(err);
			reply({text: `${err}`})
		}else reply({text: 'Token sent'});
	} );
};

const example1 = srv.info.uri+'/auth/email/auth-by-email-demo@yopmail.com';
const example2 = srv.info.uri+'/auth/email/auth-by-email-demo@yopmail.com/'+(new Buffer(srv.info.uri+'/auth/user-info').toString('base64'));
const route = {
	method: ["POST"], path: "/auth/email/{email}/{b64url?}",
	config: {
		auth: false,
		validate: {
			query: {
				email:"an email address you can consult",
				url:"where to redirect after authentication validation. optional"
			}
		},
		payload: {
			parse: true,
		},
		/*validate: {
			query: Joi.object({
				param1: Joi.string().insensitive().required()
			})
		},*/
		description: "request authentification by email with optional base64 encoded redirection url",
		notes:'<strong>Test me :</strong><ul>' +
		'<li><a href="'+example1+'">'+example1+'</a></li>' +
		'<li><a href="'+example2+'">'+example2+'</a></li>' +
		'</ul>View and test the resulting email on this public mailbox : <a href="http://www.yopmail.com/auth-by-email-demo">http://www.yopmail.com/auth-by-email-demo</a>',
		tags: ["public"]
	},
	handler: handler
};
module.exports.handler = handler;
module.exports.route = route;
