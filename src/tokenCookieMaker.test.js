jest.mock('./api-config', () => {
	return {
		config: {
			secretKey: 'secretKey',
			sessionDuration:'10s',
			email:{
				ttl: '1s'
			}
		}
	};
});

const JWT = require('jsonwebtoken');
const app = require('./tokenCookieMaker');
const config = require('./api-config').config;

describe('tokenCookieMaker',()=>{
	function replySpyMock_builder(spyObject){
		return json => {
			spyObject.reply = json;
			spyObject.reply.state = (a,b,c)=>{
				spyObject.reply.stateRes = {cookieName:a,cookieContent:b,cookieOptions:c};
				return spyObject.reply;
			};
			return spyObject.reply;
		};
	}
	it('create valid cookie', () => {
		const spy = {};
		app(replySpyMock_builder(spy)({}),{test:'test'});
		expect(JWT.verify(spy.reply.stateRes.cookieContent, config.secretKey).test).toBe('test');
	});
	it('update cookie with new expire time', () => {
		const spy = {};
		app(replySpyMock_builder(spy)({}),{exp:0});
		expect(JWT.verify(spy.reply.stateRes.cookieContent, config.secretKey)).toBeTruthy();
	});
	it('handle human readable expire time', () => {
		const spy = {};
		app(replySpyMock_builder(spy)({}),{});
		expect(JWT.verify(spy.reply.stateRes.cookieContent, config.secretKey)).toBeTruthy();
	});
});
