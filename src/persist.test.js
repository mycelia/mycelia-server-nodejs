const app = require('./persist');
const fse = require('fs-extra');
const git = require('simple-git/promise');

const backupConsoleLog = console.log;
const backupConsoleError = console.error;

describe('persist',()=>{
	let config;
	beforeEach( ()=>{
		config = {
			dataFolder:"generated.local/"
		};
	});
	afterEach( ()=>{
		fse.removeSync(config.dataFolder);
	});
	describe('init fail cases',()=>{
		beforeEach( ()=>{
			console.log = () => 'nothing';
			console.error = () => 'nothing';
		});
		afterEach( ()=>{
			console.log = backupConsoleLog;
			console.error = backupConsoleError;
		});
		it('no target', () => {
			expect.assertions(1);
			expect(app.init(config)).rejects.toBeTruthy();
		});
		it('target inert (not valid git root)', async () => {
			expect.assertions(1);
			config = {dataFolder:"../generated.sandbox.test/"};
			await fse.remove(config.dataFolder);
			await fse.ensureFile(config.dataFolder+'.gitkeep');
			await expect(app.init(config)).rejects.toBeTruthy();
		});
		it('app sha and data sha identicals', async () => {
			expect.assertions(1);
			fse.mkdirsSync(config.dataFolder);
			await expect(app.init(config)).rejects.toBeTruthy();
		});
		it('should be fine', async () => {
			expect.assertions(1);

			await fse.ensureFile(config.dataFolder+'.gitkeep');
			await git(config.dataFolder).init();
			await git(config.dataFolder).addConfig('user.email','test@test.test');
			await git(config.dataFolder).addConfig('user.name','test');
			await git(config.dataFolder).add(['.']);
			await git(config.dataFolder).commit('init');

			await expect(app.init(config)).resolves.toBeTruthy();
		});
	});
	describe('save checks and process',()=>{
		let asUser,fromCommit,firstCommit,filesMap,accessRightHandler;
		const refDataFolder = 'generated.ref.git/';
		async function prepareOnce(){
			// create persistence sandbox folder
			await fse.outputFile(config.dataFolder+'the.file','aaa\naaa\naaa\naaa\n');
			await git(config.dataFolder).init();
			await git(config.dataFolder).addConfig('user.email','test@test.test');
			await git(config.dataFolder).addConfig('user.name','test');
			await git(config.dataFolder).add(['.']);
			await git(config.dataFolder).commit('init');
			firstCommit = (await git(config.dataFolder).revparse(['HEAD'])).trim();
			//await git(config.dataFolder).raw('config --bool core.bare true'.split(' '));
			//fse.copySync(config.dataFolder+'.git/',refDataFolder);
			await git(config.dataFolder).addConfig('receive.denyCurrentBranch','updateInstead');
			fse.copySync(config.dataFolder,refDataFolder);
			fse.removeSync(config.dataFolder);
		}
		beforeEach( async ()=>{
			if(!await fse.pathExists(refDataFolder)) await prepareOnce();
			await fse.copy(refDataFolder,'generated.distant/');
			await git().clone('generated.distant/','generated.local/');
			await git(config.dataFolder).addConfig('receive.denyCurrentBranch','updateInstead');

			if(!firstCommit) firstCommit = (await git(config.dataFolder).revparse(['HEAD'])).trim();
			fromCommit = firstCommit;
			asUser = {user:'test',email:'test@test.test'};
			filesMap = {'an/empty.file':''};
			accessRightHandler = {
				canCommit: async (thisPath)=>true
			};
			await app.init(config,accessRightHandler);
		});
		afterEach(()=> fse.removeSync('generated.distant/') )
		it('allow to commit when granted', async () => {
			expect.assertions(1);
			await expect( app.save(asUser,fromCommit,filesMap) ).resolves.toEqual('200: saved');
		},10000);
		it('deny commit when not allowed', async () => {
			expect.assertions(1);
			accessRightHandler.canCommit = async (thisPath)=>false ;
			await expect( app.save(asUser,fromCommit,filesMap) ).rejects.toEqual('403: Insufficient Rights');
		},10000);
		async function getLastCommitUserEmail(gitRoot) {
			return (await git(gitRoot).raw('log --format=%aE HEAD'.split(' '))).split('\n')[0].trim();
		}
		it('credit the commit to the given user', async () => {
			expect.assertions(2);
			await expect( getLastCommitUserEmail(config.dataFolder) ).resolves.toEqual('test@test.test');
			asUser = {user:'someone',email:'some@one.mail'};
			await app.save(asUser,fromCommit,filesMap);
			await expect( getLastCommitUserEmail(config.dataFolder) ).resolves.toEqual('some@one.mail');
		},10000);
		it('write expecteds files', async () => {
			expect.assertions(2);
			filesMap = {'the/path/to.file':'content','an/other/path/to.file':'content\nmultiLine'};
			await app.save(asUser,fromCommit,filesMap);

			expect( (await fse.readFile(config.dataFolder+'the/path/to.file')).toString() ).toEqual('content');
			expect( (await fse.readFile(config.dataFolder+'an/other/path/to.file')).toString() ).toBeTruthy();
		},10000);
		xit('handle multiples nearly synchronous commit', async () => {
			expect.assertions(2);
			let a = app.save(asUser,fromCommit,{'the/path/to.file':'a'});
			let b = app.save(asUser,fromCommit,{'an/other/path/to.file':'b'});
			await a; await b;
			expect( (await fse.readFile(config.dataFolder+'the/path/to.file')).toString() ).toEqual('a');
			expect( (await fse.readFile(config.dataFolder+'an/other/path/to.file')).toString() ).toEqual('b');
		},10000);
		it('merge successive commits from same ref commit', async () => {
			expect.assertions(1);
			await app.save(asUser,fromCommit,{'the.file':'aaa\nbbb\naaa\naaa\n'});
			await app.save(asUser,fromCommit,{'the.file':'aaa\naaa\naaa\nccc\n'});
			expect( (await fse.readFile(config.dataFolder+'the.file')).toString() ).toMatch(/^aaa[\r\n]+bbb[\r\n]+aaa[\r\n]+ccc[\r\n]+$/);
		},10000);
		it('fail to merge unmergeable', async () => {
			console.log = () => 'nothing';
			console.error = () => 'nothing';

			expect.assertions(2);
			await app.save(asUser,fromCommit,{'the.file':'b'});
			await expect( app.save(asUser,fromCommit,{'the.file':'c'}) ).rejects.toBeTruthy();
			expect( (await fse.readFile(config.dataFolder+'the.file')).toString() ).toEqual('b');

			console.log = backupConsoleLog;
			console.error = backupConsoleError;
		},10000);
		it('push to distant repo', async () => {
			expect.assertions(1);

			filesMap = {'an/empty.file':''};
			await app.save(asUser,fromCommit,filesMap);

			expect( await fse.exists('generated.distant/an/empty.file') ).toBe(true);
		},10000);
		//FIXME: write a test to enlight overlaping issue security hole --> use dedicated clone to prepare commit without overwrite risk and security holes.
	});
});
