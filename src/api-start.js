const srv = require('./api-init-server');
const config = require('./api-config').config;

srv.start(function () {
	console.log('Mycelia edit srv running at:', srv.info.uri);
	console.log('Mycelia dataFolder:', config.dataFolder);
	//try{ require("openurl").open(srv.info.uri); } catch(e){}
});
