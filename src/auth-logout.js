const tokenCookieRemover = require('./tokenCookieRemover');

const route = {
	method: "GET", path: "/auth/logout",
	config: {
		auth: 'jwt',
		description: "log you out (by erasing your authentification cookie)",
		notes:'<strong>Test me : </strong><a href="/auth/logout'+'">/auth/logout'+'</a>',
		tags: ["registered only"]
	},
	handler: function (request, reply) {
		tokenCookieRemover(reply({text: 'Bye'}));
	}
};
module.exports.route = route;
