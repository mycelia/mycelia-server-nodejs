const config = require('./api-config').config;
const JWT = require('jsonwebtoken');
const ms = require('ms');

const cookie_options = {
	ttl: typeof config.sessionDuration === 'number' ? config.sessionDuration : ms(config.sessionDuration, {toNumber:true}),
	encoding: 'none',    // we already used JWT to encode
	isSecure: false,      // warm & fuzzy feelings //TODO : importer le paramètre depuis la config pour avoir des cookies securisé en prod.
	isHttpOnly: true,    // prevent client alteration
	clearInvalid: true, // remove invalid cookies
	strictHeader: true,  // don't allow violations of RFC 6265
	path: '/'            // set the cookie for all routes
};


module.exports = (reply, content)=> {
	if(typeof content.exp !== 'undefined') delete content.exp; // expire
	const longTimeSessionToken = JWT.sign(content, config.secretKey, {expiresIn: config.sessionDuration});
	return reply.state("token", longTimeSessionToken, cookie_options);
};
