const {config, users} = require('./api-config');
if (!config.secretKey) throw 'secretKey required in config.yml';

function validate(decoded, request, callback) {
	//FIXME: demander les droits d'accès à accessRight.js plutôt que de les calculer ici.
	const email = decoded.email;
	const userGroup = [];
	for (let group in users) {
		if (users[group].indexOf(email) !== -1) userGroup.push(group);
	}
	if (userGroup.length) {
		decoded.user = email.split('@')[0];
		decoded.access = userGroup;
		return callback(null, true, decoded);
	}
	else return callback(null, false);
}
const jwtAuthStrategyConf = {
	key: config.secretKey, // add other api pub keys here  // Never Share your secret key
	validateFunc: validate,            // validate function defined above
	verifyOptions: {algorithms: ['HS256', 'RS256']} // pick a strong algorithm
};
module.exports = jwtAuthStrategyConf;
