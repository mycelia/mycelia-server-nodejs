//process.env.PORT = 3000;
const http = require('http');
jest.mock('./api-config', () => {
	return {
		config: {
			dataFolder: 'data.example/',
			secretKey: 'secretKey',
			sessionDuration: 10000,
			email: {
				ttl: '1s'
			}
		},
		users: {
			admin: ['the@admin'],
			thisRight: ['me@me', 'the@admin'],
			thatOne: ['the@admin', 'me@me'],
			andThatToo: ['me@me']
		}
	};
});
async function startSrv() {
	const srvService = await require('../index').start();
	return srvService.hapiSrv;
}
async function send(onSrv, pathUrl, cookies, data) {
	return await new Promise((resolve, reject) => {
		let postData='';
		const headers = {};
		const reqObj = {'host': onSrv.info.host, 'port': onSrv.info.port, 'headers': headers, 'path': pathUrl};
		if (cookies) headers.Cookie = cookies;
		if (data) {
			reqObj.method = 'POST';
			for(let key in data) postData += key+'='+JSON.stringify(data[key])+'\n';
			headers['Content-Type'] = 'application/x-www-form-urlencoded';
			headers['Content-Length'] = Buffer.byteLength(postData);
		}
		let result = '';
		let req = http.request(reqObj,
			(response) => {
				response.setEncoding('utf8');
				response.on('data', (chunk) => result += chunk);
				response.on('end', () => resolve(result));
			}
		);
		if (data) req.write(postData);
		req.end();
	});
}

const backupConsoleLog = console.log;
const backupConsoleError = console.error;
describe('e2e', () => {
	beforeEach( ()=>{
		console.log = () => 'nothing';
		console.error = () => 'nothing';
	});
	afterEach( ()=>{
		console.log = backupConsoleLog;
		console.error = backupConsoleError;
	});
	it('/auth/user-info', async () => {
		const srv = await startSrv();

		const JWT = require('jsonwebtoken');
		const config = require('./api-config').config;

		const cookies = 'token='+JWT.sign({email:'me@me'}, config.secretKey, {expiresIn: config.sessionDuration});
		const path = '/auth/user-info';
		const result = await send(srv,path,cookies);
		expect(JSON.parse(result).access).toEqual(["thisRight","thatOne","andThatToo"]);
		srv.stop();
	});
});
