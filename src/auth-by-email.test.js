jest.mock('./api-config', () => {
	return {
		config: {
			secretKey: 'secretKey',
			email:{ttl: '1s'}
		}
	};
});
jest.mock('emailjs', () => {
	let spyHost ={};
	return {
		setSpy:(spy,willFail=false)=> {spyHost.email = spy;spyHost.fail=willFail},
		message:{ create: (json)=>spyHost.email.messageCreate = json },
		server: {
			connect: (json)=>{
				spyHost.email.serverConnect = json;
				const noError = spyHost.fail;
				return { send:(json,callback)=> callback(noError,spyHost.email.send = json) };
			}
		}
	};
});

const JWT = require('jsonwebtoken');
const email = require('emailjs');
const config = require('./api-config').config;
const app = require('./auth-by-email');

describe('auth-by-email',()=>{
	let request;
	let reply;
	const spy={email:{}};
	beforeEach( ()=>{
		request = {
			params:{},
			payload:{},
			connection:{info:{protocol:'https'}},
			info:{host:'domain.name'}
		};
		reply = (json)=> spy.reply = json ;
		email.setSpy(spy.email);
	});
	it('handle email server error when trying to send email', () => {
		const backupConsoleLog = console.log;
		console.log = () => 'nothing';

		const willFail= "Error: timeout";
		email.setSpy(spy.email, willFail);
		request.params.email = 'to@email';
		app.handler(request,reply);
		expect(spy.reply.text.toString()).toBe('Error: timeout');

		console.log = backupConsoleLog;
	});
	it('send email', () => {
		request.params.email = 'to@email';
		app.handler(request,reply);
		expect(spy.reply.text.toString()).toBe('Token sent');
		expect(spy.email.send.to).toBe(request.params.email);
	});
	it('send email with valid jwt token', () => {
		app.handler(request,reply);
		const token = spy.email.send.attachment[0].data.match(/href="([^"]+)"/)[1].split('/').pop();
		expect(JWT.verify(token, config.secretKey)).toBeTruthy();
	});
	it('send email with redirection url when needed', () => {
		request.params.b64url = new Buffer('redirectUrl').toString('base64');
		app.handler(request,reply);
		const tokenContent = JWT.verify(spy.email.send.attachment[0].data.match(/href="([^"]+)"/)[1].split('/').pop(), config.secretKey);
		expect(tokenContent.url).toBe('redirectUrl');
	});
	it('send email with custom sender and replyTo when given', () => {
		config.email.sender = 'Trust me, i send it <s@nd.r>';
		config.email.replyTo = 'reply@to.me';
		app.handler(request,reply);
		expect(spy.email.send.from).toBe(config.email.sender);
		expect(spy.email.send['reply-to']).toBe(config.email.replyTo);
	});
});
