jest.mock('./api-config', () => {
	return {
		config: {
			secretKey: 'secretKey',
			email:{ttl: '1s'}
		}
	};
});
const app = require('./auth-by-email');

describe('auth-by-email',()=>{
	let request;
	let reply;
	beforeEach( ()=>{
		request = { params:{},payload:{},connection:{info:{}},info:{} };
		reply = (json)=> {} ;
	});
	it('fail to send email if missing config data', () => {
		const spy = [];
		global.console = {log:(a)=> spy.push(a.toString())};
		app.handler(request,reply);
		expect(spy[0]).toBe("Error: message does not have a valid sender");
	});
});
