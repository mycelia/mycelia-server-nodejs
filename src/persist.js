const git = require('simple-git/promise');
const fse = require('fs-extra');

let config,accesRightHandler;
async function init(importConfig,importAccesRightHandler) {
	config = importConfig;
	accesRightHandler = importAccesRightHandler;
	const shaData = git(importConfig.dataFolder).silent(true).revparse(['HEAD']);
	let shaCode = git(__dirname).silent(true).revparse(['HEAD']);
	if(__dirname.indexOf('node_modules')!== -1) shaCode = null; //FIXME: shaCode devrais pouvoir être null si l'instance du server d'édition n'est pas versionnée. -> écrire le test correspondant
	await git(config.dataFolder).addConfig('receive.denyCurrentBranch','updateInstead');
	if(await shaData && await shaData !== await shaCode) return await shaData;
	throw {
		error:'Error: persistence init fail : unable to interact with persistence layer',
		'shaData':`${await shaData} for ${importConfig.dataFolder}`,
		'shaCode':`${await shaCode} for ${process.cwd()}`
	};
}
async function save(asUser,fromCommit,filesMap) {
	const tempRepo = '/tmp/mycelia/'+asUser.user+Math.random()+'/';
	await git(config.dataFolder).pull('origin','master');
	await fse.ensureDir(tempRepo);
	await git().clone(config.dataFolder,tempRepo);
	await git(tempRepo).raw(["reset","--hard",fromCommit]);
	const futurs = [];
	futurs.push(git(tempRepo).addConfig('user.name',asUser.user));
	futurs.push(git(tempRepo).addConfig('user.email',asUser.email));
	//futurs.push(git(config.dataFolder).addConfig('push.default','simple'));
	futurs.push(git(tempRepo).addConfig('core.mergeoptions','--no-edit'));

	// check filesMap files syntaxe
	//TODO: explode data.yml in folder and files
	//TODO: extract from config.yml layerConfig
	// apply filesMap
	for(let filesPath in filesMap) futurs.push(fse.outputFile( tempRepo+filesPath, filesMap[filesPath] ));
	for(let f of futurs) await f;

	await git(tempRepo).add(['.']);

	if( await accesRightHandler.canCommit(tempRepo)){
		await git(tempRepo).commit(`${asUser.user} contrib`);
		//pull
		await tryPush(tempRepo,3); // push change to local

		await fse.remove(tempRepo);

		await tryPush(config.dataFolder,3,'fail to push on distant origin'); // push to distant

		return '200: saved';
	}
	await fse.remove(tempRepo);
	throw '403: Insufficient Rights';
}

module.exports = {init,save};
async function tryPush(gitRepo,times,customError = 'fail to reach a pushable state'){
	if(times === 0) return Promise.reject(customError);
	try{
		await git(gitRepo).pull('origin','master');
		await git(gitRepo).push('origin','master');
	} catch (e){
		await wait(Math.round(Math.random()*1000));
		return await tryPush(gitRepo,times-1);
	}
	//console.log('after push : \n',await git(gitRepo).raw(["log","--all","--graph","--pretty=format:'%h %cr <%an> %d %s'"]));
	const localRev = git(gitRepo).revparse(['HEAD']);
	const remoteRev = git(gitRepo).revparse(['origin/master']);
	if((await localRev) === (await remoteRev)) return Promise.resolve('changes pushed');
	//return Promise.reject('merge conflict');
}
function wait(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
