const tokenCookieMaker = require('./tokenCookieMaker');
const tokenCookieRemover = require('./tokenCookieRemover');
const JWT = require('jsonwebtoken');
const config = require('./api-config').config;

const handler = function (request, reply) {
	try {
		const content = validate(request.params.jwt);
		if (content.url){
			const url = JSON.parse(content.url);
			delete content.url;
			tokenCookieMaker(reply({text: 'OK'}), content).redirect(url);
		}
		else tokenCookieMaker(reply({text: 'OK'}), content);
	} catch (err) {
		tokenCookieRemover(reply({text: err}));
	}
};
const example1 = '/auth/jwt/'+JWT.sign({email:"auth-by-email-demo@yopmail.com"}, config.secretKey, {expiresIn: config.email.ttl});
const example2 = '/auth/jwt/'+JWT.sign({email:"auth-by-email-demo@yopmail.com"}, config.secretKey, {expiresIn: "1ms"});
const example3 = '/auth/jwt/'+JWT.sign({email:"auth-by-email-demo@yopmail.com"}, "wrongKey", {expiresIn: config.email.ttl});
const example4 = '/auth/jwt/'+JWT.sign({email:"unknown@email.com"}, config.secretKey, {expiresIn: config.email.ttl});

const route = {
	method: ["GET"], path: "/auth/jwt/{jwt}",
	config: {
		auth: false,
		description: "check your authentification jwt and attach you session cookie if success",
		notes:'<strong>Test me :</strong><ul>' +
		'<li><a href="'+example1+'">valid token</a></li> (try to use it twice it will have burned. refresh for a new-one.)' +
		'<li><a href="'+example2+'">too old token</a></li>' +
		'<li><a href="'+example3+'">untrusted token</a></li>' +
		'<li><a href="'+example4+'">unknown user token</a></li>' +
		'</ul>',
		tags: ["public"]
	},
	handler: handler
};
module.exports.handler = handler;
module.exports.route = route;

function validate(token){
	const decoded = JWT.verify(token, config.secretKey);
	if (isValid(token)) {
		burn(token);
		return decoded;
	} else throw "TokenBurntError: already used jwt token";
}
const burnedTokens = [];
function burn(token) {
	removeObsoleteTokensFromBurned();
	burnedTokens.push(token);
}
const isValid = (token) => burnedTokens.indexOf(token) === -1 ;
function removeObsoleteTokensFromBurned() {
	for (let i = burnedTokens.length - 1; i >= 0; i--) {
		try {
			JWT.verify(request.params.jwt, config.secretKey); //FIXME: include public keys
		} catch (err) {
			burnedTokens.splice(i, 1);
		}
	}
}
