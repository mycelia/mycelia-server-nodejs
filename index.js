const rawExec = require('child_process').exec;
const pmx = require("pmx").init({
//	network       : true,  // Network monitoring at the application level
	ports: true,  // Shows which ports your app is listening on (default: false)
});

function ReloadableSrv() {
	const self = this;
	self.start = async (options) => {
		process.options = options;
		return new Promise((resolve, reject) => {
			//FIXME: clear node require caches
			Object.keys(require.cache).forEach((i) => delete require.cache[i]);
			self.hapiSrv = require('./src/api-init');
			self.hapiSrv.once('start', () => resolve(self));
			const config = require('./src/api-config').config;
			if (config.srvUpdateCheckFrequency) {
				if(typeof config.srvUpdateCheckFrequency !== 'number'){
					config.srvUpdateCheckFrequency = require('ms')(config.srvUpdateCheckFrequency, {toNumber:true});
				}
				const updateIn = config.srvUpdateCheckFrequency * 0.9 + Math.random()*config.srvUpdateCheckFrequency * 0.2;
				setTimeout(self.update, updateIn);
			}
		});
	};
	self.update = async () => {
		console.log("start server update process");
		await exec("node_modules/.bin/mycelia update", process.options.dataFolder);
		await exec("node_modules/.bin/pm2 update", process.options.dataFolder);
		await exec("pm2 reload mycelia-server-nodejs", process.options.dataFolder);
	};
	/*self.reload = () => {
		console.error('not implemented yet');
		// start new srv on tmp port
		// if everything is ok :
		// switch port
		// stop old srv
		// else display errorsport
	};
	self.restart = async () => {
		await self.stop(false);
		await self.start(process.options);
	};*/
	self.stop = async (exiting = true) => {
		await self.hapiSrv.stop({timeout: 60000});
		if (exiting) process.exit(0);
	};
	process.on('SIGINT', function () {
		self.stop();
	});
	process.on('message', function (msg) {
		if (msg === 'shutdown') self.stop();
	});
	pmx.action('start', async (param, reply) => reply({answer: await self.start(param)}));
	pmx.action('stop', async (reply) => reply({answer: await self.stop()}));
	pmx.action('update', async (reply) => reply({answer: await self.update()}));
	return self;
}

module.exports = new ReloadableSrv();

async function exec(cmd, cwd, quiet = false) {
	return new Promise((resolve, reject) => {
		const childProcess = rawExec(cmd, {
			cwd: cwd,
			maxBuffer: 10 * 1024 * 1024
		}, (err, stdout, stderr) => {
			if (err) reject({err: err, stderr: stderr, stdout: stdout});
			else resolve(stdout);
		});
		if (!quiet) {
			childProcess.stdout.on('data', (data) => {
				console.log(`| ${data}`.trim());
			});
			childProcess.stderr.on('data', (data) => {
				console.error(`| err: ${data}`.trim());
			});

		}
	});

}
