/**
 * Created by elisa on 12/06/17.
 */
//------------------------------------INIT------------------------------------------------------------------------------
const childProcess= require('child_process');
//------------------------------------COULEUR---------------------------------------------------------------------------
function badgeColor(percent) {
	let color = 'blue';
	if(percent>0) color = 'brightgreen';
	if(percent>20) color = 'green';
	if(percent>35) color = 'yellowgreen';
	if(percent>50) color = 'yellow';
	if(percent>70) color = 'orange';
	if(percent>85) color = 'red';
	return color;
}
//---------------------------MANIPULATION-REPORT------------------------------------------------------------------------
function buildBadgeFromReport(err,stdout,stderr) {
	console.log('error: ',err);
	console.log('stdout: ',stdout);
	console.log('stderr: ',stderr);

	const fs = require("fs");
	const stringReport = fs.readFileSync(__dirname+'/generated.jscpdReport.json');
	const jsonReport = JSON.parse(stringReport);
	const score = Math.round(parseFloat(jsonReport.statistics.percentage));
	console.log('jscpd duplciation score :', score);
	let color = badgeColor(score);
	childProcess.execSync('wget -q -O generated.jscpd-duplication.svg https://img.shields.io/badge/duplication-'+score+'%-'+color+'.svg',{cwd:__dirname});
}


//---------------------------APPEL-DES-FONCTIONS------------------------------------------------------------------------
childProcess.exec('jscpd --config jscpdConfig.yml',{cwd:__dirname} , buildBadgeFromReport );
