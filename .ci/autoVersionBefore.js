#!/usr/bin/env node
const fs = require('fs');

doIt();
async function doIt() {

	const pkg = JSON.parse(fs.readFileSync(`${process.cwd()}/package.json`, 'utf8'));
	const msg = fs.readFileSync(`${process.cwd()}/.git/COMMIT_EDITMSG`, 'utf8');
	if(msg.search(/FEAT*:/i) !== -1) pkg.version = bump(pkg.version,'minor');
	else if(msg.search(/FIX*:/i) !== -1) pkg.version = bump(pkg.version,'patch');
	//else process.exit(1);
	fs.writeFileSync(`${process.cwd()}/package.json`, JSON.stringify(pkg,null,2));
	fs.writeFileSync(`${process.cwd()}/.git/needAmend`, 'true');
}

function bump(originalVersion,bumpType){
	const versionArray = originalVersion.split('.');
	if(bumpType === 'minor'){
		versionArray[1]++;
		versionArray[2] = 0;
	}
	if(bumpType === 'patch') versionArray[2]++;
	return versionArray.join('.');
}
