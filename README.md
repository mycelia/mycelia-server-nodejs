[![build status](https://framagit.org/mycelia/mycelia-server-nodejs/badges/master/build.svg)](https://framagit.org/mycelia/mycelia-server-nodejs/pipelines)
[![coverage report](https://framagit.org/mycelia/mycelia-server-nodejs/badges/master/coverage.svg)](https://mycelia.tools/ci/mycelia-server-nodejs/coverage/)
[![duplication](https://mycelia.tools/ci/mycelia-server-nodejs/duplication/generated.jscpd-duplication.svg)](https://mycelia.tools/ci/mycelia-server-nodejs/duplication/generated.jscpd-duplication)

Versionned, authored, authentified storage rest api (git and paswordless based)

# Installation recommandée :

Sur un serveur (Linux Ubuntu-server alors que j'écrit ces lignes)
- assurez vous que soit installé git version 2.3 ou superieur, et nodejs version 8.1.0 ou superieur.
- assurez vous d'avoir une clef ssh privé accepté par le gitlab qui héberge votre projet.
- `git clone git@...votreProjet.git`
- `cd votreProjet`
- assurez-vous que le fichier config-srv.yml contient bien : 
  - l'url ou les url de consultation du projet (pour autoriser les requetes cors de cette provenance).
  - de quoi se connecter à un serveur email valide.
  - le port sur le quel il doit écouter.
  - (optionnel) le domaine par le quel il sera accessible en ligne.
- `npm install`
- `npm run server`
 
                       


## Notes :

ergonomie front pour l'authentification :

serveur (pré rempli avec le serveur d'édition par défaut)
identifiant (placeholder:email) (a saisir par l'utilisateur)
mot de passe (placeholder:plus de sécurité, moins de mot de passe, authentifiez vous par email)
connexion


si l'identifiant est un email, retirer ou désactiver le champ mot de passe au profit du placeholder
si l'identifiant n'est pas un email, suggérer lesspass et authentifier par mot de passe.
