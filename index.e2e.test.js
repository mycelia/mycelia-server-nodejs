const app = require('./index');
describe('e2e', () => {
/*	const backupConsoleLog = console.log;
	beforeEach(()=> console.log = () => 'nothing');
	afterEach(()=> console.log = backupConsoleLog);*/
	it('launcher can start and stop server', async () => {
		expect(app.hapiSrv).toBeFalsy();
		await app.start();
		expect(app.hapiSrv.info.started).toBeTruthy();
		await app.stop(false);
		expect(app.hapiSrv.info.started).toBeFalsy();
	});
	xit('launcher can start and stop server again', async () => {
		await app.start();
		expect(app.hapiSrv.info.started).toBeTruthy();
		await app.stop(false);
		expect(app.hapiSrv.info.started).toBeFalsy();
	});
	xit('launcher can restart server', async () => {
		await app.start({port:65000});
		process.options.port = 64000;
		expect(app.hapiSrv.info.port).toBe(65000);
		await app.restart();
		expect(app.hapiSrv.info.port).toBe(64000);
		await app.stop(false);
	});
});
